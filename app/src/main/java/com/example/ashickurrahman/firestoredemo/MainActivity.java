package com.example.ashickurrahman.firestoredemo;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {


    EditText name,age,contact;
    Button save,view,updateBtn;
    FirebaseFirestore db;
    TextView allText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = FirebaseFirestore.getInstance();

        save=findViewById(R.id.savebtn);
        view=findViewById(R.id.viewbtn);
        name=findViewById(R.id.nametext);
        age=findViewById(R.id.agefield);
        contact=findViewById(R.id.contact);
        allText=findViewById(R.id.viewdata);
        updateBtn=findViewById(R.id.updatebtn);


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Map<String, Object> user = new HashMap<>();
                user.put("name", name.getText().toString());
                user.put("age", age.getText().toString());
                user.put("contact", contact.getText().toString());

                db.collection("users")
                        .add(user)
                        .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                            @Override
                            public void onSuccess(DocumentReference documentReference) {
                                Log.d("logsss", "DocumentSnapshot added with ID: " + documentReference.getId());
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.w("logsss", "Error adding document", e);
                            }
                        });

            }
        });

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.collection("users")
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    Log.d("logss", document.getId() + " => " + document.getData());
                                    String nameD= (String) document.get("name");
                                    String ageD= (String) document.get("age");
                                    String contactD=(String) document.get("contact");

                                    allText.setText(nameD+" "+ageD+" "+contactD+"");


                                }
                            } else {
                                Log.w("logss", "Error getting documents.", task.getException());
                            }
                        }
                    });
            }
        });

        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String srchName=name.getText().toString();
                db.collection("users")
                        .whereEqualTo("name",srchName)
                        .get()
                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                if (task.isSuccessful()) {
                                    for (QueryDocumentSnapshot document : task.getResult()) {
                                        Log.d("logss", document.getId() + " => " + document.getData());
                                        String nameD= (String) document.get("name");
                                        String ageD= (String) document.get("age");
                                        String contactD=(String) document.get("contact");

                                        allText.setText(nameD+" "+ageD+" "+contactD+"");


                                    }
                                } else {
                                    Log.w("logss", "Error getting documents.", task.getException());
                                }
                            }
                        });
            }
        });
    }
}
